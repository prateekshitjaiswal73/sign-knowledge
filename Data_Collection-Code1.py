import cv2
from cvzone.HandTrackingModule import HandDetector
import numpy as np
import math

cap = cv2.VideoCapture(0)
# Initialize the HandDetector class with the given parameters
detector = HandDetector(staticMode=False, maxHands=2, modelComplexity=1, detectionCon=0.5, minTrackCon=0.5)

# Setting offset for getting extra pixels near the hands
offset = 20

# Setting image size for normalizing the hand size
imgSize = 300

#detector = HandDetector(maxHands=2)
# Continuously get frames from the webcam
while True:
    # Capture each frame from the webcam
    # 'success' will be True if the frame is successfully captured, 'img' will contain the frame
    success, img = cap.read()

    # Find hands in the current frame
    # The 'draw' parameter draws landmarks and hand outlines on the image if set to True
    # The 'flipType' parameter flips the image, making it easier for some detections
    hands, img = detector.findHands(img, draw=True, flipType=True)

    # Check if any hands are detected
    if len(hands) == 2:
        # Information for the first hand detected
        hand1 = hands[0]  # Get the first hand detected
        x1, y1, w1, h1 = hand1["bbox"]  # Bounding box around the first hand (x1, y1, w1, h1 coordinates)
        handType1 = hand1["type"]  # Type of the first hand ("Left" or "Right")

        imgWhite1 = np.ones((imgSize, imgSize, 3), np.uint8) * 255 # Adjusting the image captured to a proper size
        imgCrop1 = img[y1 - offset:y1 + h1 + offset, x1 - offset:x1 + w1 + offset]

        imgCropShape1 = imgCrop1.shape

        aspectRatio1 = h1 / w1
        if aspectRatio1 > 1:
            k1 = imgSize / h1
            wCal1 = math.ceil(k1 * w1)
            imgResize1 = cv2.resize(imgCrop1, (wCal1, imgSize))
            imgResizeShape1 = imgResize1.shape
            wGap1 = math.ceil((imgSize - wCal1) / 2)
            imgWhite1[:, wGap1:wCal1 + wGap1] = imgResize1
        else:
            k1 = imgSize / w1
            hCal1 = math.ceil(k1 * h1)
            imgResize1 = cv2.resize(imgCrop1, (imgSize, hCal1))
            imgResizeShape1 = imgResize1.shape
            hGap1 = math.ceil((imgSize - hCal1) / 2)
            imgWhite1[hGap1:hCal1 + hGap1,:] = imgResize1


        # Information for the second hand
        hand2 = hands[1]
        x2, y2, w2, h2 = hand2["bbox"]
        handType2 = hand2["type"]

        imgWhite2 = np.ones((imgSize, imgSize, 3), np.uint8) * 255
        imgCrop2 = img[y2 - offset:y2 + h2 + offset, x2 - offset:x2 + w2 + offset]


        imgCropShape1 = imgCrop1.shape

        aspectRatio2 = h2 / w2
        if aspectRatio2 > 1:
            k2 = imgSize / h2
            wCal2 = math.ceil(k2 * w2)
            imgResize2 = cv2.resize(imgCrop2, (wCal2, imgSize))
            imgResizeShape2 = imgResize2.shape
            wGap2 = math.ceil((imgSize - wCal2) / 2)
            imgWhite2[:, wGap2:wCal2 + wGap2] = imgResize2
        else:
            k2 = imgSize / w2
            hCal2 = math.ceil(k2 * h2)
            imgResize2 = cv2.resize(imgCrop2, (imgSize, hCal2))
            imgResizeShape2 = imgResize2.shape
            hGap2 = math.ceil((imgSize - hCal2) / 2)
            imgWhite2[hGap2:hCal2 + hGap2,:] = imgResize2




        cv2.imshow("ImageCrop1", imgCrop1)
        cv2.imshow("ImageWhite1", imgWhite1)

        cv2.imshow("ImageCrop2", imgCrop2)
        cv2.imshow("ImageWhite2", imgWhite2)


    #imgWhite = np.ones((imgSize, imgSize * 2, 3), np.uint8) * 255

    cv2.imshow("Image", img)
    cv2.waitKey(1)
