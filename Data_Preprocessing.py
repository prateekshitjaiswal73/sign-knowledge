import os
import cv2
import numpy as np
from sklearn.model_selection import train_test_split
from tensorflow.keras.utils import to_categorical

# Constants
data_dir = "./Images"  # Directory containing subfolders for each class
img_size = (500, 500)  # Target image size (adjust as needed)
num_classes = 3  # Number of classes (A-Z)

# Function to load and preprocess images
def preprocess_image(file_path):
    img = cv2.imread(file_path)
    img = cv2.resize(img, img_size)
    img = img / 255.0  # Normalize pixel values to [0, 1]
    return img

# Initialize lists to store images and labels
X = []  # Images
y = []  # Labels

# Loop through class folders and collect images and labels
for class_label in os.listdir(data_dir):
    class_folder = os.path.join(data_dir, class_label)
    if os.path.isdir(class_folder):
        class_images = os.listdir(class_folder)
        for image_file in class_images:
            image_path = os.path.join(class_folder, image_file)
            if image_path.endswith(".jpg"):
                image = preprocess_image(image_path)
                X.append(image)
                y.append(ord(class_label) - ord('A'))  # Map class labels to integers

# Convert lists to numpy arrays
X = np.array(X)
y = np.array(y)

# Split the dataset into training, validation, and test sets
X_train, X_temp, y_train, y_temp = train_test_split(X, y, test_size=0.3, random_state=42)
X_val, X_test, y_val, y_test = train_test_split(X_temp, y_temp, test_size=0.5, random_state=42)

# Convert class labels to one-hot encoded vectors
y_train = to_categorical(y_train, num_classes)
y_val = to_categorical(y_val, num_classes)
y_test = to_categorical(y_test, num_classes)

# Save the preprocessed data (images and labels) for future use
np.save("X_train.npy", X_train)
np.save("y_train.npy", y_train)
np.save("X_val.npy", X_val)
np.save("y_val.npy", y_val)
np.save("X_test.npy", X_test)
np.save("y_test.npy", y_test)

print("Data preprocessing completed.")

