import cv2
from cvzone.HandTrackingModule import HandDetector
import numpy as np
import math
import time

cap = cv2.VideoCapture(0)
detector = HandDetector(maxHands=2)  # Allow detection of up to 2 hands

# Setting offset for getting extra pixels near the hands
offset = 20

# Setting image size for normalizing the hand size
imgSize = 500

counter = 0
folder = "./Images/A"


while True:
    success, img = cap.read()
    hands, img = detector.findHands(img)

    if hands:
        # Initialize coordinates for a bounding box that encompasses all detected hands
        min_x, min_y, max_w, max_h = float('inf'), float('inf'), 0, 0

        for hand in hands:
            x, y, w, h = hand["bbox"]
            min_x = min(min_x, x)
            min_y = min(min_y, y)
            max_w = max(max_w, x + w)
            max_h = max(max_h, y + h)

        # Crop the image to include all hands with some extra space (offset) around them

        imgCrop = img[min_y - offset:max_h + offset, min_x - offset:max_w + offset]

        imgWhite = np.ones((imgSize, imgSize, 3), np.uint8) * 255 # Adjusting the image captured to a proper size

        imgCropShape = imgCrop.shape

        aspectRatio = h / w
        if aspectRatio > 1:
            k = imgSize / h
            wCal = math.ceil(k * w)
            imgResize = cv2.resize(imgCrop, (wCal, imgSize))
            imgResizeShape = imgResize.shape
            wGap = math.ceil((imgSize - wCal) / 2)
            imgWhite[:, wGap:wCal + wGap] = imgResize
        else:
            k = imgSize / w
            hCal = math.ceil(k * h)
            imgResize = cv2.resize(imgCrop, (imgSize, hCal))
            imgResizeShape = imgResize.shape
            hGap = math.ceil((imgSize - hCal) / 2)
            imgWhite[hGap:hCal + hGap,:] = imgResize




        cv2.imshow("ImageCrop", imgCrop)
        cv2.imshow("ImageWhite", imgWhite)


    cv2.imshow("Hand Detection", img)
    key = cv2.waitKey(1)
    if key == ord("s"):
        counter += 1
        cv2.imwrite(f"{folder}/A_{time.time()}.jpg", imgWhite)
        print(counter)

